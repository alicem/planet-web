### Feed Addition Request

(Following the instructions the Planet GNOME Wiki page on adding your feed: https://wiki.gnome.org/PlanetGnome#Being_added_to_Planet_GNOME)

 - Provide your full name
 - Provide your IRC nickname
 - Provide a link to your blog
 - Provide a link to your valid RSS or Atom feed

Provide a hackergotchi (they will be reviewed to check if they match the guidelines: https://wiki.gnome.org/PlanetGnome/HackergotchiGuidelines), unless you really don't want to have a hackergotchi.

/label ~FeedAddition